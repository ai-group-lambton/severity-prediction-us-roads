import streamlit as st
from reports_page import display_reports
from prediction_form import display_prediction_form

# Set page configuration
st.set_page_config(layout="wide")

#1. Setting up the background Image of the Web page
# Custom CSS to set the background color to grey and increase text size
page_bg_img =( """
    <style>
    [data-testid="stAppViewContainer"] {
        background-image: url("https://images.unsplash.com/photo-1520013817300-1f4c1cb245ef?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTV8fG9uZSUyMGNvbG9yfGVufDB8fDB8fHww");
        background-size: cover;
    }
    .markdown-text-container {
        font-size: 20px;
    }
    </style>
"""



               )
st.markdown(page_bg_img, unsafe_allow_html=True)

# Define the icon image path
icon_path = "../Images/carlogos.png"  # Replace this with the path to your image file

# Display the icon image and the title on the same row
col1, col2 = st.columns([2, 7])
with col1:
    st.image(icon_path, width=300)  # Adjust the width as needed
with col2:
    st.write("")
    st.write("")
    st.write("")
    st.write("")
    st.markdown("<h1 style='text-align: left; color: black; font-size: 60px;'>Traffic Accident Severity Prediction</h1>", unsafe_allow_html=True)

# Apply custom styling to the title
st.markdown("""<style> .css-1kcmnvl { font-family: 'Georgia', sans-serif !important; } </style>""", unsafe_allow_html=True)



# Custom CSS to style the header text
header_css = """
    <style>
    [data-testid="StyledLinkIconContainer"] {
        /* Change the color of the header text to grey */
        .header-span 
            color: grey; /* Change the color to your desired shade of grey */
        }
    </style>
"""

# Display the custom CSS using st.markdown()
st.markdown(header_css, unsafe_allow_html=True)


# Display the header with custom color
st.markdown("<h1 style='text-align: left; color: dark grey; font-size: 40px;'>Protecting Your Future, Securing Your Life</h1>", unsafe_allow_html=True)

st.write("""
    <div style='font-size: 18px; color: dark grey; font-family: "Calibri", sans-serif; text-align: justify;'>
        Welcome to our Web Application, your solution for predicting accident severity in traffic flow. 
        Designed for insurance professionals like you, our application combines cutting-edge algorithms and data
        analysis to deliver real-time predictions on accident severity.
        With this application, you can proactively assess risks, expedite claims processing, and 
        enhance decision-making. 
        
        
    </div>
""", unsafe_allow_html=True)

st.write("")
st.write("")
st.write("")
st.write("")


col1, col2 = st.columns(2)
with col1:
    # Boxed content for "Our Insurance Products"
    with st.expander("**EXPLORING OUR OFFERINGS**",expanded=True):
        st.markdown("<h1 style='text-align: left; color: dark grey; font-size: 15px;'>WHAT WE PROVIDE</h1>", unsafe_allow_html=True)


        st.write("""
        - Our intuitive interface and customizable features empower you to tailor the app to your workflow, ensuring maximum efficiency and accuracy.
        - Thank you for choosing our web application. We're excited to have you on board. 
        - Explore our offerings and discover the right insurance solutions tailored to your needs. 
        - If you have any questions or need assistance, don't hesitate to contact us. Enjoy your experience!
    """)
        st.image("../Images/us.png", caption="Maximizing Protection with Excess Insurance", width=650,)

with col2:
    # Boxed content for "Why Choose Us?"
    with st.expander("**WHY CHOOSE US**",expanded=True):
        st.markdown("<h1 style='text-align: left; color: dark grey; font-size: 15px;'>PROFESSIONAL KNOWLEDGE PROFICIENCY</h1>", unsafe_allow_html=True)

        st.write("""
        - **Friendly Dashboard**: Our app offers a user-friendly dashboard that simplifies navigation and enhances user experience. You can access all essential features and functionalities with ease, making it effortless to manage and analyze data.
        - **Customization of Prediction System Parameters**: We understand that every insurance company has unique needs. With our app, you can customize prediction system parameters to align with your specific requirements. This level of customization ensures that you get accurate and relevant predictions tailored to your business model.
        - **Prediction Severity Visualization**: Visualizing prediction severity is crucial for informed decision-making. Our app provides intuitive visualization tools that allow you to explore and understand severity predictions effortlessly. Clear graphs and charts make it easy to interpret data and take proactive measures.
        - **Adjustment with Accuracy and Interpretability**: Flexibility is key in the insurance industry. Our app allows you to adjust parameters on the go, empowering you to fine-tune predictions based on real-time insights. Whether it's adjusting risk thresholds or refining prediction models, you have full control at your fingertips.
        """)
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")
        st.write("")





st.write("")
st.write("")
st.write("")
st.write("")

# Display the header for Prediction Process
st.markdown("<h1 style='text-align: left; color: dark grey; font-size: 40px;'>Prediction Process</h1>", unsafe_allow_html=True)

# Display the content for Prediction Process
st.write("""
    <div style='font-size: 18px; color: dark grey; font-family: "Calibri", sans-serif; text-align: justify;'>
        Our severity prediction system analyzes various factors, including accident location, weather conditions, and historical data. Machine learning models trained on vast datasets enable us to predict the severity of accidents with high accuracy. These predictions assist insurance adjusters and underwriters in evaluating claims, estimating reserves, and pricing policies effectively.
        <br><br>
        <strong>Benefits:</strong>
        <ul>
            <li><strong>Improved Risk Assessment:</strong> Accurate severity predictions enables our customer to assess risk more precisely, leading to better-informed decisions.</li>
            <li><strong>Resource Allocation:</strong> By anticipating the severity of accidents, customers can allocate resources such as claims adjusters and emergency services more efficiently.</li>
            <li><strong>Cost Containment:</strong> Early identification of high-severity accidents allows customers to mitigate potential losses and manage claim payouts effectively.</li>
            <li><strong>Enhanced Customer Service:</strong> Timely and accurate severity predictions contribute to faster claims processing and improved customer satisfaction.</li>
        </ul>
    </div>
""", unsafe_allow_html=True)
st.write("")
st.write("")
st.write("")
st.write("")
# Boxed content for "Severity Prediction Form"
with ((st.expander("**FORECASTING MADE SIMPLE**", expanded=True))):

    # Split the page into two columns
    col1, col2 = st.columns(2)

    # Display prediction form in the first column
    with col1:

        severity_prediction, input_data = display_prediction_form()



    with col2:

        if severity_prediction is not None:
            st.markdown("<h1 style='text-align: left; color: dark grey; font-size: 15px;'>PREDICTION RESULT</h1>", unsafe_allow_html=True)
            # Concatenate the text and severity prediction into a single string
            prediction_text = f"<h3 style='color:dark grey; font-family: Arial, sans-serif;'>Severity Level Impact: {severity_prediction}</h3>"
            st.markdown(prediction_text, unsafe_allow_html=True)




            if severity_prediction == 1:
                st.markdown("""
                    ### MINIMAL IMPACT
                   <div style="font-family: Calibri, sans-serif; color: #333333; text-align: justify;">
                    Accidents classified as Severity Level 1 indicate minimal impact or risk. These accidents typically involve minor injuries or property damage with low financial implications. Examples include minor fender benders, small property damage incidents, or low-speed collisions.
                 </div>
                    """, unsafe_allow_html=True)

            elif severity_prediction == 2:
                st.markdown("""
        ### LOW IMPACT
        <div style="font-family: Calibri, sans-serif; color: #333333; text-align: justify;">
        Severity Level 2 accidents suggest a low level of impact or risk. These accidents may involve slightly more significant injuries or property damage but still have manageable financial consequences. Examples include moderate fender benders, minor injuries requiring medical attention, or single-vehicle accidents.
        </div>
        """, unsafe_allow_html=True)

            elif severity_prediction == 3:
                st.markdown("""
        ### MODERATE IMPACT
        <div style="font-family: Calibri, sans-serif; color: #333333; text-align: justify;">
        Severity Level 3 accidents indicate a moderate level of impact or risk. These accidents involve moderate to severe injuries, substantial property damage, and higher financial implications. Examples include multi-vehicle collisions, injuries requiring hospitalization, or accidents resulting in significant property damage.
        </div>
        """, unsafe_allow_html=True)

            elif severity_prediction == 4:
                st.markdown("""
        ### HIGH IMPACT
        <div style="font-family: Calibri, sans-serif; color: #333333; text-align: justify;">
        Severity Level 4 accidents represent the highest level of impact or risk. These accidents involve severe injuries, fatalities, extensive property damage, and significant financial losses. Examples include catastrophic collisions, fatalities, total loss of vehicles, or accidents resulting in extensive property damage and liability claims.
        </div>
        """, unsafe_allow_html=True)
            st.image("../Images/severity.png", caption="Severity Impact Levels")

#save_prediction_to_history(input_data, severity_prediction)



st.write("")
st.write("")
st.write("")
st.write("")

# Boxed content for "Reports"
with st.expander("**REPORTS**", expanded=True):  # Set expanded=True to expand by default
    with st.container():
        display_reports()

st.write("")
st.write("")
st.write("")
st.write("")

# Define the CSS style for the image container
image_container_style = (
    "max-width: 100%;"  # Ensure the image fits within the container width
)
st.markdown("<h2 style='text-align: left; color: dark grey; font-size: 30px;'>Get in Touch</h2>", unsafe_allow_html=True)
st.write("""
    <div style='font-size: 18px; color: dark grey; font-family: "Calibri", sans-serif; text-align: left;'>
        Ready to take the first step towards securing your future? <br>
        Stay ahead of the curve with comprehensive reports. Our app generates detailed reports that highlight
        key insights in accident severity predictions. Use this valuable information 
        to make strategic decisions and optimize your insurance operations.
       
    </div>
""", unsafe_allow_html=True)
st.write("")

# Display the image
st.markdown(
    f"""
    <div style="{image_container_style}">
        <img src="https://w0.peakpx.com/wallpaper/244/100/HD-wallpaper-business-analysis-insurance-technology-services.jpg" alt="Insurance Image" style="width: 100%;">
    </div>
    """,
    unsafe_allow_html=True
)

# Display the contact information below the image
st.write("")
st.markdown("""
    <div style='font-size: 15px; color: dark grey; font-family: "Georgia", sans-serif; text-align: left;'>
        Contact us today to embark on your journey towards a secure and prosperous future.
        <strong>Phone</strong>: 1-800-123-4567 
        <strong>Email</strong>: group02Project@insurance.com 
        <strong>Visit Us</strong>: Brunel Road, Mississauga, Canada 
    </div>
""", unsafe_allow_html=True)