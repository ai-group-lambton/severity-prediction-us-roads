import streamlit as st
import pandas as pd
import joblib
import os
import re

import requests
# Define the path to the prediction history CSV file
PREDICTION_HISTORY_FILE = '../History/prediction_history.csv'
# Preprocessing function for one-hot encoding categorical variables
from uszipcode import SearchEngine
from pyzipcode import ZipCodeDatabase

def validate_inputs(street, county, zipcode, weather_condition,start_time,end_time,start_date,end_date,distance):
    if not street:
        st.error("Please enter a street address.")
        return False
    if not county:
        st.error("Please enter a county.")
        return False
    pattern = r'^\d{5}$'
    if not zipcode or not re.match(pattern, zipcode):
        st.error("Please enter a valid 5 digit US zipcode.")
        return False
    if not weather_condition:
        st.error("Please enter a weather condition.")
        return False
    if not validate_date(start_date):
        st.error("Please enter a valid start date in YYYY-MM-DD format.")
        return False
    if not validate_date(end_date):
        st.error("Please enter a valid end date YYYY-MM-DD format.")
        return False
    if start_date > end_date:
        st.error("Start date cannot be greater than end date.")
        return False
    if start_date == end_date and start_time > end_time:
        st.error("Start time cannot be greater than end time for the same date.")
        return False
    if not validate_time(start_time):
        st.error("Please enter a valid start time.")
        return False
    if not validate_time(end_time):
        st.error("Please enter a valid end time.")
        return False
    if not validate_positive_number(distance):
        st.error("Please enter a positive number for distance.")
        return False
    return True

def validate_time(time_str):
    pattern = r'^([01]\d|2[0-3]):([0-5]\d)$'
    if not time_str or not re.match(pattern, time_str):
        return False
    return True

def validate_date(date_str):
    pattern = r'^\d{4}-\d{2}-\d{2}$'
    if not date_str or not re.match(pattern, date_str):
        return False
    return True

def validate_positive_number(number):
    if number <= 0:
        return False
    return True

def get_day_or_night(start_time):
    if pd.isnull(start_time):
        return False  # Return False if start_time is missing
    # Convert start time to timestamp
    start_time_ts = pd.Timestamp.combine(pd.Timestamp.today(), pd.to_datetime(start_time).time())
    # Define day and night time ranges
    day_start = pd.Timestamp.combine(pd.Timestamp.today(), pd.to_datetime('06:30:00').time())
    day_end = pd.Timestamp.combine(pd.Timestamp.today(), pd.to_datetime('18:30:00').time())
    # Check if start time corresponds to the correct day or night period
    if day_start <= start_time_ts <= day_end:
        return True  # Start time is during the day
    else:
        return False  # Start time is during the night
def get_zip_info(zip_code):
    # Check if the ZIP code is valid in the US
    search = SearchEngine()
    result_us = search.by_zipcode(zip_code)

    # Check if the ZIP code is valid in Canada
    zip_db = ZipCodeDatabase()
    try:
        result_ca = zip_db[zip_code]
        is_valid_ca = result_ca is not None
    except KeyError:
        # Handle the case where the ZIP code is not found in the Canadian database
        is_valid_ca = False

    if result_us is not None and result_us.zipcode is not None:
        # If the ZIP code is valid in the US, return US information
        return (
            result_us.major_city,
            result_us.state,
            "United States",
            result_us.timezone,
        )
    elif is_valid_ca:
        # If the ZIP code is valid in Canada, return Canada information
        return (
            result_ca.city,
            result_ca.state,
            "Canada",
            None,  # Canadian ZIP code database does not include timezone info
        )
    else:
        # If the ZIP code is not valid in either US or Canada, return sample data
        return ("Sample City", "Sample State", "Sample Country", "Sample Timezone")

def choose_model(option):
    random_forest = joblib.load('../Dataset/rfm.joblib')
    gradient_boost = joblib.load('../Dataset/gbm.joblib')
    combined_model = joblib.load('../Dataset/best_xgb_2.joblib')
    if option=="Accuracy":
        return random_forest
    elif option=="Interpretability":
        return gradient_boost
    else:
        return combined_model
def preprocess_input(df):
    # Define all categorical columns used during training
    categorical_cols = ['Street', 'City', 'County', 'State', 'Zipcode', 'Country', 'Timezone','Start_Date','End_Date'
        ,'Wind_Direction', 'Weather_Condition', 'Sunrise_Sunset','Start_Time','End_Time']

    # Check if 'Sunrise_Sunset' column exists in the DataFrame
    if 'Sunrise_Sunset' in df.columns:
        # One-hot encode categorical variables
        df_encoded = pd.get_dummies(df, columns=categorical_cols, drop_first=True)
    else:
        # If 'Sunrise_Sunset' column is not present, create an empty DataFrame with all categorical columns
        df_encoded = pd.DataFrame(columns=categorical_cols)

    # Ensure all columns present in the training data are also present in the input data
    # Replace these columns with the actual columns used during model training
    expected_columns = ['Distance(mi)', 'Street', 'City', 'County', 'State', 'Zipcode', 'Country', 'Timezone',
                        'Wind_Direction', 'Weather_Condition', 'Amenity', 'Bump', 'Crossing', 'Give_Way',
                        'Junction', 'No_Exit', 'Railway', 'Roundabout', 'Station', 'Stop', 'Traffic_Calming',
                        'Traffic_Signal', 'Turning_Loop', 'Sunrise_Sunset', 'Start_Date', 'End_Date',
                        'Start_Time', 'End_Time']

    for col in expected_columns:
        if col not in df_encoded.columns:
            df_encoded[col] = 0

    # Reorder columns to match the order in the training data
    df_encoded = df_encoded[expected_columns]

    return df_encoded

def save_prediction_to_history(data, prediction):
    # Create or append to the prediction history CSV file
    if not os.path.exists(PREDICTION_HISTORY_FILE):
        df = pd.DataFrame(columns=['Date and Time', 'Location', 'Actual Severity', 'Predicted Severity'])
        df.to_csv(PREDICTION_HISTORY_FILE, index=False)
    # Append the new prediction(s) to the CSV file
    columns_to_select = ['Start_Date', 'End_Date']  # Specify the columns to select
    selected_columns = [data[col].iloc[0] for col in columns_to_select]  # Select the specified columns
    selected_columns.append(prediction)  # Append the prediction column
    if len(selected_columns) == 3:  # Ensure the correct number of columns
        with open(PREDICTION_HISTORY_FILE, 'a') as file:
            file.write(','.join(map(str, selected_columns)) + '\n')
    else:
        print("Error: Incorrect number of columns selected.")

def display_prediction_form():
    # Prediction Form
    st.markdown("<h1 style='text-align: left; color: dark grey; font-size: 15px;'>PREDICTION FORM</h1>", unsafe_allow_html=True)



    # Input fields for each feature
    col1, col2 = st.columns(2)

    with col1:
        street =st.text_input("Street")
        county = st.text_input("County")
    with col2:
        zipcode = st.text_input("Zipcode")
        weather_condition = st.text_input("Weather Condition")
    cola,colb= st.columns(2)
    with cola:
        traffic_signal = st.checkbox("Traffic Signal")
        crossing = st.checkbox("Crossing")
    with colb:
        junction = st.checkbox("Junction")
        turning_loop = st.checkbox("Turning Loop")

    col1,col2 = st.columns(2)
    with col1:
        start_date = st.text_input("Start Date YYYY-MM-DD")
        start_time = st.text_input("Start Time HH:MM:SS")

    with col2:
        end_date = st.text_input("End Date YYYY-MM-DD")
        end_time = st.text_input("End Time HH:MM:SS")

    distance = st.number_input("Distance (mi)", min_value=0)
    option=st.selectbox("What is more important to you?",['Accuracy','Interpretability','Combination of Accuracy and Interpretability'])

    # Submit button
    if st.button("Submit"):
        if not validate_inputs(street, county, zipcode, weather_condition,start_time,end_time,start_date,end_date,distance):
            return None, None
        sunrise_sunset=get_day_or_night(start_time)
        sunrise_sunset_numeric = 1 if sunrise_sunset == True else 0
        # Create a DataFrame from the input values
        city, state, country, timezone = get_zip_info(zipcode)
        data = {
            'Distance(mi)': [distance],
            'Street': [street],
            'City': [city],
            'County': [county],
            'State': [state],
            'Zipcode': [zipcode],
            'Country': [country],
            'Timezone': [timezone],
            'Wind_Direction': ["NSW"],
            'Weather_Condition': [weather_condition],
            'Amenity': [0],
            'Bump': [1],
            'Crossing': [1 if crossing else 0],
            'Give_Way': [1],
            'Junction': [1 if junction else 0],
            'No_Exit': [1],
            'Railway': [1],
            'Roundabout': [0],
            'Station': [1],
            'Stop': [0],
            'Traffic_Calming': [0],
            'Traffic_Signal': [1 if traffic_signal else 0],
            'Turning_Loop': [1 if turning_loop else 0],
            'Sunrise_Sunset': [sunrise_sunset_numeric],
            'Start_Date': [start_date],
            'End_Date': [end_date],
            'Start_Time': [start_time],
            'End_Time': [end_time]
        }

        # Display the DataFrame
        df = pd.DataFrame(data)
        df_encoded = preprocess_input(df)
        # Make predictions
        model = choose_model(option)
        predictions = model.predict(df_encoded)
        return predictions[0], df

    # Return None if the form hasn't been submitted yet
    return None, None